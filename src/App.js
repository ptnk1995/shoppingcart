import React from 'react';
import Title from './components/Title';
import ProductList from './components/ProductList';
import Cart from './components/Cart';

import './App.css';

function App() {
  return (
    <div className="container">
      <Title/>
      <div className="row">
        <ProductList/>
        <Cart />
      </div>
    </div>
  );
}

export default App;
