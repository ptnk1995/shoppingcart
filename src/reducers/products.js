import * as types from '../constants/ActionType';

let defaultState = [
    {
        id:     'a1',
        name: 'aplusautomation',
        image: 'aplusautomation.jpg',
        summary: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dicta asperiores veniam repellat unde debitis quisquam magnam magni ut deleniti!',
        price: 12,
        canBuy: true
    }, 
    {
        id:     'a2',
        name: 'aplus media',
        image: 'aplus-media.jpg',
        summary: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dicta asperiores veniam repellat unde debitis quisquam magnam magni ut deleniti!',
        price: 2,
        canBuy: false
    }, {
        id:     'a3',
        name: 'aplusautomation 3',
        image: 'aplusautomation.jpg',
        summary: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dicta asperiores veniam repellat unde debitis quisquam magnam magni ut deleniti!',
        price: 23,
        canBuy: true
    }, 
];

const products = (state = defaultState, action) => {
    switch (action.type) {
        case types.LIST_PRODUCT:
            return state;
        default:
            return state;
    }
}

export default products;