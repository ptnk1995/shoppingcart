import React, { Component } from 'react';
import { connect } from 'react-redux';

import Helpers from './../libs/Helpers';
import { actRemoveProduct, actChangeNotify, actUpdateProduct } from '../actions';
import * as configs from './../constants/Config';
import Validate from '../libs/Validate';

class CartItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 0
        };
    }

    handleUpdate = (product, quantity) => {
        if (Validate.checkQuantity(+quantity) === false) {
            this.props.changeNotify(configs.NOTI_GREATER_THAN_ONE);
        } else {
            this.props.updateProduct(product, +quantity);
            this.props.changeNotify(configs.NOTI_ACT_UPDATE);
        }
    }

    handleDelete = (product) => {
        this.props.removeProduct(product);
        this.props.changeNotify(configs.NOTI_ACT_DELETE);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        let { cartItem, index } = this.props;
        let { product } = cartItem;
        let quantity = (this.state.value != 0) ? this.state.value : cartItem.quantity;
        // let quantity = cartItem.quantity || this.state.value;

        return (
            <tr>
                <th scope="row">{index + 1}</th>
                <td>{product.name}</td>
                <td>{Helpers.toCurrency(product.price, "$", "right")}</td>
                {/* <td></td> */}
                <td><input name="value" type="number" value={quantity} min={1} onChange={this.handleChange} /></td>
                <td>
                    <strong>{this.showSubTotal(product.price, quantity)}</strong>
                </td>
                <td>
                    <a className="label label-info update-cart-item" onClick={() => this.handleUpdate(product, quantity)}
                        href="http://localhost:3000/#" data-product>Update</a>
                    <a className="label label-danger delete-cart-item" onClick={() => this.handleDelete(product)}
                        href="http://localhost:3000/#" data-product>Delete</a>
                </td>
            </tr>
        );
    }

    showSubTotal(price, quantity) {
        let result = null;
        result = Helpers.toCurrency(price * quantity, "$", "right");
        return result;
    }
}

// export default CartItem;

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        updateProduct: (product, quantity) => {
            dispatch(actUpdateProduct(product, quantity));
        },
        removeProduct: (product) => {
            dispatch(actRemoveProduct(product));
        },
        changeNotify: (value) => {
            dispatch(actChangeNotify(value));
        }
    }
}

export default connect(null, mapDispatchToProps)(CartItem);